use serde::{Serialize, Deserialize};
use super::restaurant::*;

/// Response Structure with a status success and a message.
#[derive(Serialize, Deserialize)]
pub struct ResponseMessage{
    pub success : bool,
    pub message : String
}

#[derive(Serialize, Deserialize)]
pub struct ResponseMessageClient {
    pub success : bool,
    pub message : String,
    pub clients : Vec<Client>
}

#[derive(Serialize, Deserialize)]
pub struct ResponseMessageDish {
    pub success : bool,
    pub message : String,
    pub dishes : Vec<Dish>
}

#[derive(Serialize, Deserialize)]
pub struct ResponseMessageCommand {
    pub success : bool,
    pub message : String,
    pub commandes : Vec<Command>
}