use serde::{Serialize, Deserialize};

#[derive(Serialize,Deserialize, Debug)]
pub struct Command {
    pub id          : u32,
    pub id_dish     : u32,
    pub id_client   : u32,
    pub quantity    : u32
}

#[derive(Serialize,Deserialize, Debug)]
pub struct Dish {
    pub id      : u32,
    pub name    : String,
    pub size    : u32
}

#[derive(Serialize,Deserialize, Debug)]
pub struct Client {
    pub id      : u32
}