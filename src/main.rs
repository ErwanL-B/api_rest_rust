mod models;
mod api;
mod databases;

use actix_web::{App, HttpServer};
use actix_cors::Cors;

#[actix_web::main]
async fn main() -> std::io::Result<()>{

    let host: String = "127.0.0.1".to_string();
    let port: String = "8900".to_string();

    let server = HttpServer::new(||{
        /*let cors = Cors::default()
                .allowed_origin("http://127.0.0.1:8700/")
                .allowed_methods(vec!["GET", "POST"])
                .allowed_headers(vec![http::header::AUTHORIZATION, http::header::ACCEPT])
                .allowed_header(http::header::CONTENT_TYPE)
                .max_age(3600);*/
        let cors = Cors::default().allow_any_origin().send_wildcard();

        App::new()
            .wrap(cors)
            .configure(api::config::config_api)
    })
    .bind(format!("{}:{}", host, port))?;

    server.run().await
}

