use actix_web::{HttpResponse, Responder, get, web, post, delete};
use crate::api::functions::web::Json;
use crate::databases::database::*;
use crate::models::restaurant::*;
use crate::models::response_api::{ResponseMessage, ResponseMessageClient, ResponseMessageDish, ResponseMessageCommand};

// File used by api to call database
#[get("/")] 
async fn index_data() -> impl Responder
{
    HttpResponse::Ok().body("Hello World from Data!")
}

#[get("add_client")]
pub async fn add_client() -> impl Responder {
    let response: ResponseMessage = create_client();
    HttpResponse::Ok().json(response)
}

#[get("get_clients")]
pub async fn get_clients() -> impl Responder {
    let response : ResponseMessageClient = get_clients_from_db();
    HttpResponse::Ok().json(response)
}

#[get("get_client/{id}")]
pub async fn get_client(id_client : web::Path<u32>) -> impl Responder {
    let id = id_client.to_string().parse::<u32>().unwrap();
    let response : ResponseMessageClient = get_client_from_db(id);
    HttpResponse::Ok().json(response)
}

#[delete("del_client/{id}")]
pub async fn del_client(id : web::Path<u32>) -> impl Responder {
    let response : ResponseMessage = delete_client(id.to_string().parse::<u32>().unwrap());
    HttpResponse::Ok().json(response)
}

#[post("add_dish")]
pub async fn add_dish(new_dish : Json<Dish>) -> impl Responder {
    let response: ResponseMessage = create_dish(new_dish.into_inner());
    HttpResponse::Ok().json(response)
}

#[get("get_dish/{id}")]
pub async fn get_dish(id : web::Path<u32>) -> impl Responder {
    let id_dish = id.to_string().parse::<u32>().unwrap();
    let response : ResponseMessageDish = get_dish_from_db(id_dish);
    HttpResponse::Ok().json(response)
}

#[get("get_dishes")]
pub async fn get_dishes() -> impl Responder {
    let response : ResponseMessageDish = get_dishes_from_db();
    HttpResponse::Ok().json(response)
}


#[post("add_command")]
pub async fn add_command(new_com : Json<Command>) -> impl Responder {
    let response: ResponseMessage = create_command(new_com.into_inner());
    HttpResponse::Ok().json(response)
}

#[get("get_command/{id}")]
pub async fn get_command(id : web::Path<u32>) -> impl Responder {
    let id_com = id.to_string().parse::<u32>().unwrap();
    let response : ResponseMessageCommand = get_command_from_db(id_com);
    HttpResponse::Ok().json(response)
}

#[get("get_commandes")]
pub async fn get_commandes() -> impl Responder {
    let response : ResponseMessageCommand = get_commandes_from_db();
    HttpResponse::Ok().json(response)
}

#[delete("del_command/{id}")]
pub async fn del_command(id : web::Path<u32>) -> impl Responder {
    let id_com = id.to_string().parse::<u32>().unwrap();
    let response : ResponseMessage = delete_command(id_com);
    HttpResponse::Ok().json(response)
}

// CLIENTS

/// Create a client in the restaurant.
fn create_client() -> ResponseMessage {
    let mut db : DatabaseConnection = DatabaseConnection::new() ;
    db.insert_query_client()
}

fn get_clients_from_db() -> ResponseMessageClient{
    let mut db : DatabaseConnection = DatabaseConnection::new() ;
    db.select_query_clients()
}

fn get_client_from_db(id : u32) -> ResponseMessageClient {
    let mut db : DatabaseConnection = DatabaseConnection::new();
    println!("ID du client demandé : {} ",id);
    db.select_query_client(id)
}

fn delete_client(id_cli : u32) -> ResponseMessage {
    let mut db : DatabaseConnection = DatabaseConnection::new() ;
    db.delete_query_client(id_cli)
}

// DISH

/// Create a client in the restaurant.
fn create_dish(d : Dish) -> ResponseMessage {
    let mut db : DatabaseConnection = DatabaseConnection::new() ;
    db.insert_query_dish(d)
}

fn get_dish_from_db(id : u32) -> ResponseMessageDish {
    let mut db : DatabaseConnection = DatabaseConnection::new() ;
    db.select_query_dish(id)
}

fn get_dishes_from_db() -> ResponseMessageDish {
    let mut db : DatabaseConnection = DatabaseConnection::new();
    db.select_query_dishes()
}

// COMMAND
fn create_command(command : Command) -> ResponseMessage{
    let mut db : DatabaseConnection = DatabaseConnection::new();
    db.insert_query_command(command)
}

fn get_command_from_db(id_com : u32) -> ResponseMessageCommand {
    let mut db : DatabaseConnection = DatabaseConnection::new();
    db.select_query_command(id_com)
}

fn get_commandes_from_db() -> ResponseMessageCommand {
    let mut db : DatabaseConnection = DatabaseConnection::new();
    db.select_query_commands()
}

fn delete_command(id : u32) -> ResponseMessage {
    let mut db : DatabaseConnection = DatabaseConnection::new();
    db.delete_query_command(id)
}