use actix_web::{HttpResponse, Responder, get};
use crate::models::response_api::ResponseMessage;

#[get("/")] 
async fn index() -> impl Responder
{
    HttpResponse::Ok().body("Hello World !")
}

#[get("health")]
async fn health_check() -> impl Responder{
    let response = ResponseMessage{
        success : true,
        message : String::from("Everything is fine !")
    } ;
    HttpResponse::Ok().json(response)
}

pub async fn not_found() -> impl Responder{
    let response : ResponseMessage = ResponseMessage{
        success : false,
        message : String::from("Ressource not found...")
    } ;
    HttpResponse::NotFound().json(response)
} 