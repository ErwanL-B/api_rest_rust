use actix_web::{web};
use super::functions::*;
use super::default::*;

pub fn config_api(cfg : &mut web::ServiceConfig) {
    cfg
        .service(index)
        .service(health_check)
        .service(web::scope("/data")
            .service(index_data)
            .service(add_client)
            .service(del_client)
            .service(get_clients)
            .service(get_client)
            .service(add_command)
            .service(get_command)
            .service(get_commandes)
            .service(del_command)
            .service(add_dish)
            .service(get_dish)
            .service(get_dishes)
        )
        .default_service(web::route().to(not_found));
}