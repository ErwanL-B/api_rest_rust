use mysql::*;
use mysql::prelude::*;

use crate::models::response_api::*;
use crate::models::restaurant::*;

pub struct DatabaseConnection{
    pub conn : PooledConn
}

impl DatabaseConnection {
    /**
    Etablish a connection with the database MYSQL.
    */
    pub fn new() -> Self{
        let url : &str = "mysql://root:root@localhost:3306/jonrestaurant";
        let pool : Result<Pool>  = Pool::new(url);
        let nconn = pool.unwrap().get_conn().unwrap();
        DatabaseConnection { conn: nconn }
    }

    /// Get All clients of the database.
    pub fn select_query_clients(&mut self) -> ResponseMessageClient {
        let query: String = String::from("SELECT id FROM Client;");

        let result_cli = self.conn.query_map(query, |id : u32| {
            Client {id}
        });

        match result_cli {
            Ok(cs) => {
                ResponseMessageClient {
                    success : true,
                    message : String::from(""),
                    clients : cs
                }
            },
            Err(r) => {
                ResponseMessageClient {
                    success : false,
                    message : format!("Erreur : Pas de client dans la base de données.\n
                                        Erreur remontée : {}", r.to_string()),
                    clients : Vec::new()
                }
            }
        }
    }

    /// Get one client by its ID in the database.
    /// Parameter id_to_find -> u32 : ID of the client.
    pub fn select_query_client(&mut self, id_to_find : u32) -> ResponseMessageClient {
        let query: String = format!("SELECT * FROM Client WHERE id = {}", id_to_find);

        let result_cli = self.conn.query_map(query, |id : u32| {
            Client {id}
        });

        match result_cli {
            Ok(cs) => {
                ResponseMessageClient {
                    success : true,
                    message : String::from(""),
                    clients : cs
                }
            },
            Err(r) => {
                ResponseMessageClient {
                    success : false,
                    message : format!("Erreur : Pas de client dans la base de données.\n
                                        Erreur remontée : {}", r.to_string()),
                    clients : Vec::new()
                }
            }
        }
    }

    /// Get a dish by its ID in the database.
    pub fn select_query_dish(&mut self, id_to_find : u32) -> ResponseMessageDish {
        let query: String = format!("SELECT * FROM Dish WHERE id = {}", id_to_find);

        let result_dis = self.conn.query_map(query, |(id, name, size)| {
            Dish {id, name, size}
        });

        match result_dis {
            Ok(dis) => {
                ResponseMessageDish {
                    success : true,
                    message : String::from(""),
                    dishes : dis
                }
            },
            Err(r) => {
                ResponseMessageDish {
                    success : false,
                    message : format!("Erreur : Pas de plat dans la base de données.\n
                                        Erreur remontée : {}", r.to_string()),
                    dishes : Vec::new()
                }
            }
        }
    }

    /// Get a dish in the database.
    pub fn select_query_dishes(&mut self) -> ResponseMessageDish {
        let query: String = String::from("SELECT * FROM Dish;");

        let result_dis = self.conn.query_map(query, |(id, name, size)| {
            Dish {id, name, size}
        });

        match result_dis {
            Ok(dis) => {
                ResponseMessageDish {
                    success : true,
                    message : String::from(""),
                    dishes : dis
                }
            },
            Err(r) => {
                ResponseMessageDish {
                    success : false,
                    message : format!("Erreur : Pas de plat dans la base de données.\n
                                        Erreur remontée : {}", r.to_string()),
                    dishes : Vec::new()
                }
            }
        }
    }

    /// Get a command by its ID in the database.
    pub fn select_query_command(&mut self, id_to_find : u32) -> ResponseMessageCommand {
        let query: String = format!("SELECT * FROM Command WHERE id_com = {}", id_to_find);

        let result_com = self.conn.query_map(query, |(id_com, id_dis, id_cli, quantity)| {
            Command {id : id_com, id_dish : id_dis, id_client : id_cli, quantity : quantity}
        });

        match result_com {
            Ok(coms) => {
                ResponseMessageCommand {
                    success : true,
                    message : String::from(""),
                    commandes : coms
                }
            },
            Err(r) => {
                ResponseMessageCommand {
                    success : false,
                    message : format!("Erreur : Pas de Commande dans la base de données.\n
                                        Erreur remontée : {}", r.to_string()),
                    commandes : Vec::new()
                }
            }
        }
    }

    /// Get All Commands in the database.
    pub fn select_query_commands(&mut self) -> ResponseMessageCommand {
        let query: String = String::from("SELECT * FROM Command;");

        let result_com = self.conn.query_map(query, |(id_com, id_dis, id_cli, quantity)| {
            Command {id : id_com, id_dish : id_dis, id_client : id_cli, quantity : quantity}
        });

        match result_com {
            Ok(coms) => {
                ResponseMessageCommand {
                    success : true,
                    message : String::from(""),
                    commandes : coms
                }
            },
            Err(r) => {
                ResponseMessageCommand {
                    success : false,
                    message : format!("Erreur : Pas de Commande dans la base de données.\n
                                        Erreur remontée : {}", r.to_string()),
                    commandes : Vec::new()
                }
            }
        }
    }

    /**
    * Insert a Client in the database.
    * Argument structure_to_add : Client to add in the database.
    * Return ResponseMessage : Response with the success status and a message.
    */
    pub fn insert_query_client(&mut self) -> ResponseMessage{
        let query : String = String::from("INSERT INTO Client 
            VALUES (NULL);");

        self.conn.query_drop(query).unwrap();
        self.check_affected_row_query()
    }

    /**
    * Insert a Dish in the database.
    * Argument structure_to_add : Dish to add in the database.
    * Return ResponseMessage : Response with the success status and a message.
    */
    pub fn insert_query_dish(&mut self, structure_to_add : Dish) -> ResponseMessage {
        let query : String = format!("INSERT INTO Dish VALUES (NULL, '{}', {});"
                ,structure_to_add.name 
                ,structure_to_add.size);
        self.conn.query_drop(query).unwrap();
        self.check_affected_row_query()
    }

    /**
    * Insert a Command in the database.
    * Argument structure_to_add : Command to add in the database.
    * Return ResponseMessage : Response with the success status and a message.
    */
    pub fn insert_query_command(&mut self, structure_to_add : Command) -> ResponseMessage {
        let query : String = format!(r#"INSERT INTO Command(id_dish, id_client, quantity) 
            VALUES ({}, {}, {});"#, 
            structure_to_add.id_dish, 
            structure_to_add.id_client, 
            structure_to_add.quantity);
        self.conn.query_drop(query).unwrap();
        self.check_affected_row_query()
    }

    pub fn delete_query_client(&mut self, value_int : u32) -> ResponseMessage {
        let query : String = format!(r#"DELETE FROM Client WHERE id = {};"#, value_int);
        self.conn.query_drop(query).unwrap();
        self.check_affected_row_query()
    }

    pub fn delete_query_command(&mut self, value_int : u32) -> ResponseMessage {
        let query : String = format!(r#"DELETE FROM Command WHERE id_com = {};"#, value_int);
        self.conn.query_drop(query).unwrap();
        self.check_affected_row_query()
    }

    /// Allow to know if row has been changed.
    /// Return ResponseMessage : Response with the success status and a message.
    pub fn check_affected_row_query(&mut self) -> ResponseMessage {
        if self.conn.affected_rows() != 0 {
            ResponseMessage {
                success : true,
                message : String::from("Action effectuée avec succés !")
            }
        } else {
            ResponseMessage {
                success : false,
                message : String::from("Erreur lors de l'action de la base de données.")
            }
        }
    }

    
} 