# Document de Conception du JonRestaurant

## Exigences Fonctionnelles et non fonctionnelles

Les exigences fonctionnelles sont les suivantes. L'API doit : 

* Pouvoir accueillir les clients du restaurant. 
* Pouvoir leur proposer différents plats.
* Leur donner la possibilité de demander un nouveau plat.
* Pouvoir créer une commande entre un client et un plat.
* Pouvoir supprimer une entité (commande, client, plat) 

Les exigences non fonctionnelles sont les suivantes. L'API devrait :

* Pouvoir gérer le personnel du restaurant.
* Pouvoir gérer plus d'informations sur les clients.
* Pouvoir gérer le matériel du restaurant.

Ces différentes exigences peuvent influencer l'aspect attractif du restaurant car elles peuvent influer sur sa rapidité. Une bonne gestion de plusieurs types d'informations est le fruit du succès.

--- 

## Cas d'utilisation de l'API

L'API est utilisée principalement par la clientèle avec la possibilité pour eux d'être ajoutés et leur proposer des plats. Elle peut être utilisée aussi afin d'ajouter des plats au menu du restaurant. 

Le personnel peut l'utiliser aussi afin de voir les différentes commandes des clients. Il peut également supprimer des plats du menu et supprimer des commandes.

Ces cas d'utilisations peuvent être séparés en plusieurs étapes. 

Pour la clientèle, nous avons : 
* L'enregistrement du client
* La consultation des plats
* L'ajout d'un plat
* La commande

Pour le personnel, nous avons : 
* La consultation des plats
* La consultation des commandes
* La suppression des commandes


Pour chaque type d'action, l'API renvoie un schéma qui indique si l'action a réussi avec un éventuel message. Certaines actions peuvent également renvoyer en plus des objets, comme des plats par exemple.

## Services mis en place

Différents services respectant l'architecture micro-service sont mis en place pour que l'API du JonRestaurant fonctionne.

Nous avons 3 services : 

* L'API elle-même
* Une base de données
* Une documentation de l'API

La documentation de l'API correspond au swagger. Elle nécessite la présence du fichier swagger.yaml pour fonctionner. Ce service est indépendant des 2 autres sur l'aspect visuel. Cependant pour être utilisée, elle doit être connectée à l'API qui doit elle-même être en ligne. Cette communication correspond à une requête typique d'un utilisateur de l'API.

L'API est un serveur en langage Rust qui permet de recevoir des requêtes et d'y répondre via des JSON. Pour répondre à la majorité des requêtes, elle peut être amenée à interroger la base de données.  

La base de données contient simplement les données, il s'agît d'une base de données Mysql. Elle contient les tables suivantes : Client, Dish et Command

Le détail des tables : 
- Client
  - id
- Dish
  - id
  - name
  - size
- Command
  - id
  - id_dish
  - id_command
  - quantity

---

## L'API du JonRestaurant

Cette API est une API REST codée en Rust. Elle correspond à un serveur en constante écoute de requête afin de servir au mieux les utilisateurs.

L'API du JonRestaurant possède une interface swagger qui la documente et qui permet de la tester via des requêtes GET, POST et DELETE. 

Les entrées de l'API peuvent être de type variable (texte, chiffre ou JSON) alors que les sorties sont toutes en JSON pour la conformité.

## Gestion du projet et son déploiement

Ce projet a pour but d'être déployé sur un serveur de production contenant un Débian sans interface graphique. Pour se faire, il doit être "converti" en une image docker et déployé via Ansible sur le serveur. Etant codé en Rust, le projet pourra gérer un grand nombre de clients simultanément avec une grande performance et sécurité.

Le projet contient également un espace Jira pour la planification des tâches et pour une meilleure organisation. Ce projet possède également un Git, de ce fait, sa version peut être gérée et il peut être mis à jour facilement. 

Avant le déploiement, le projet réalise une pipeline Jenkins qui s'occupe de vérifier le code, tout en réalisant un build de l'API. 

C'est également cette pipeline qui réalise l'image docker qui doit être envoyée sur le serveur. 

## Sécurité du projet

Le moyen de sécurité actuellement mis en place correspond à du développement via le langage Rust afin de sécuriser l'accès à la base de données. 

Le choix du langage Rust permet une sécurisation sur la concurrence des threads via un gestionnaire natif de celui-ci. Rust possède également énormement d'exigences en terme de sécurité et de performance, ce qui fait que c'est un bon langage pour sécuriser son contenu de manière optimale. 

Cependant, une des vulnérabilités de l'API est qu'elle peut communiquer avec n'importe lequel des clients qui exploite l'API. C'est à dire que le serveur possède un mauvais paramétrage du CORS (*Cross-Origin Ressource Sharing*). De plus, si les exigences non fonctionnelles sont réalisées, l'API ne possède pas de https. 

Ce manque de chiffrement pourrait être une vulnérabilité si la base de données se mettait à contenir plus d'informations à propos des clients.

