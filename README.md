# API_REST_RUST

## Projet DevPRO

---

## Caractéristique du projet

Langage choisi : RUST

Base de données choisie : MYSQL

Langue choisie de programmation lors du codage : ANGLAIS

Gestionnaire de source choisi : GIT

Thème de l'API : Restaurant

Documentation : Swagger

Objectifs commerciaux : 
- Gérer des clients
- Gérer des plats
- Gérer des commandes
- Gérer du personnel

Objectifs techniques : 
- Réaliser une API REST
- Réaliser une IHM simple et efficace
- Réaliser une documentation détaillée
- Réaliser une base de données relationnelle

Mes objectifs personnels pour ce projet :
- Réaliser une gestion de projet (planification des tâches, gestionnaire de sources, etc.)
- Réaliser une gestion efficace des branches via Git
- Mettre en place une convention de codage, de nommage et de commits
- Développer un projet en Rust
- Développer un projet contenant une API REST
- Déployer un projet sur un serveur de production

---

## Ce qui a été réalisé avec succès

### Objectifs commerciaux 

**Gestion des clients** : Partiellement atteint.

L'API permet d'accueillir les clients dans le restaurant avec un identifiant seulement. Elle ne contient aucune donnée spécifique au client. 

**Gestion des plats** : Atteint. 

L'API permet de créer des plats, de les proposer aux clients ou de les supprimer par le personnel. Le prix est une donnée manquante mais non vitale, de ce fait l'objectif peut être considéré comme atteint. Les informations contenues par la base de données permet de gérer le nom des plats et leur taille en fonction d'un identifiant. 

**Gestion des commandes** : Atteint. 

L'API permet de créer des commandes, de les consulter ou de les supprimer par le personnel. Leurs informations permettent de les lier aux plats et aux clients.

### Objectifs techniques

**Réalisation d'une API REST** : Atteint.

L'API est une API REST qui peut exécuter des requêtes de type GET, POST et DELETE. De plus, celle-ci ne répond que en JSON. 

**Réalisation d'une documentation détaillée** : Atteint.

Une interface de type Swagger a été mise en place. Cela a permis de réaliser une documentation détaillée de l'API en plus d'offrir un moyen de tester des requêtes sur celle-ci.

**Réalisation d'une base de données relationnelle** : Atteint.

La base de données mise en place est une base de données MYSQL contenant des tables simples sur les différents types de structures de l'API.

### Objectifs personnels

**Réalisation d'une gestion de projet** : Partiellement Atteint. 

Le projet possède un espace Jira pour la mise en place d'une planification des tâches. Des sprints ont été réalisés sur les tâches d'un backlog. Il y a aussi un Git afin de gérer les sources.    

**Réalisation efficace d'un Git** : Partiellement Atteint.

Une notion de branche a été respectée tout le long du projet. De plus, les conventions de nommage et commits ont été respectés.

**Mise en place des conventions** : Atteint.

Le projet suit toutes les conventions de nommage et de codage pour un projet dans le langage Rust. Les commits ont tous un prefixe et un message décrivant l'élément concerné.

**Développement d'un projet en Rust** : Atteint.

Le projet fonctionne et il est codé en Rust. Il met en place une API qui peut recevoir des requêtes et les traiter efficacement. De plus, l'API possède des routes pour les requêtes et met à disposition des réponses adaptées. 

**Développement d'une API REST** : Atteint.

L'API peut recevoir des requêtes GET, POST et DELETE, chacune avec leur traitement associé. 

---

## Ce qui n'a pas été réalisé

### Objectifs commerciaux

**Gestion des clients** : Echec partiel

Il manque beaucoup d'informations stockées pour parler de "gestion". 

**Gestion du personnel** : Echec

Aucune implémentation du personnel dans le projet. 

### Objectifs techniques

**Réalisation d'une IHM simple et efficace** : Echec

Aucune implémentation d'interface utilisateur qui exploite l'API.

### Objectifs personnels

**Réalisation d'une gestion de projet** : Echec partiel.

Puisque c'est un projet individuel, aucune répartition des tâches n'a été réalisée. De plus, la description des tâches a été simplifiée sur l'espace Jira. Aucune méthode scrum n'a été appliquée.

**Réalisation efficace du Git** : Echec partiel. 

Pour la pipeline Jenkins, des commits sur le master directement ont été effectués ! Ceci est dû à un manque de compétence sur Jenkins. Il manque une branche "Développement" sur le Git.

**Déploiement du projet sur un serveur de production** : Echec.

Le déploiement sur le serveur a échoué, Jenkins n'a pas pu réaliser une image docker de l'API. Ansible n'a pas été utilisé de ce fait. 

--- 

### Contribution

Fichier HELPERS mis à disposition mais rappel ici aussi : 
- Philippe Roussille (Supervision)
- Erwan Lemière (Pipeline)


---

### Avis sur le projet

Un projet très intéressant aussi bien d'un point de vue technique, technologique et aussi du sujet (API REST).

Je suis juste triste de ne pas l'avoir mené jusqu'au bout puisque le déploiement a été un échec. 

Points positifs : 
- Langages de programmation ++++++++++++++++
- Programmation asynchrone 
- Techniques de déploiement automatique

Points à améliorer : 
- Mieux disposer mes outils de développement et de déploiement
- Efficacité et rapidité sur l'exécution et le développement du projet
- Plus de rigueur